<?php

/**
 * Fired during plugin activation
 *
 * @link       http://labs.systway.com
 * @since      1.0.0
 *
 * @package    Dhi
 * @subpackage Dhi/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Dhi
 * @subpackage Dhi/includes
 * @author     Systway <devs@systway.com>
 */
class Dhi_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
