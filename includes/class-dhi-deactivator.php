<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://labs.systway.com
 * @since      1.0.0
 *
 * @package    Dhi
 * @subpackage Dhi/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Dhi
 * @subpackage Dhi/includes
 * @author     Systway <devs@systway.com>
 */
class Dhi_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
