<?php

/**
 * Custom wordpress plugins for DigiHealth Information
 *
 * @link              http://lab.systway.com
 * @since             1.0.0
 * @package           Dhi
 *
 * @wordpress-plugin
 * Plugin Name:       DigiHealth Informer
 * Plugin URI:        https://codex.wordpress.org
 * Description:       DigiHealth information custom report.
 * Version:           1.0.0
 * Author:            Systway
 * Author URI:        http://lab.systway.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       dhi
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-dhi-activator.php
 */
function activate_dhi() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-dhi-activator.php';
	Dhi_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-dhi-deactivator.php
 */
function deactivate_dhi() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-dhi-deactivator.php';
	Dhi_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_dhi' );
register_deactivation_hook( __FILE__, 'deactivate_dhi' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-dhi.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_dhi() {

	$plugin = new Dhi();
	$plugin->run();

}
run_dhi();

/*shortcode for dhi*/
add_action( 'init', 'register_shortcodes');
function register_shortcodes(){
   add_shortcode('dhi-search', 'digihelth_informer_function');
}

function digihelth_informer_function($atts, $content = null ){
   extract(shortcode_atts(array(
      'class' => '',
      'id' => '',
   ), $atts));

   $return_string = '<div class="dhi_form_wrap">
   						<div class="form_header"><h3>Search Appliance</h3></div>
	   						<div class="main_form">
	   						    <form action=" " method="post">
	   						   		<div class="columnwidth">
	   						   			<input type="text"  name="main_keyword" value="" placeholder="Search Terms">
	   						   		</div>
	   						   		<div class="columnwidth">
		   						   		<select name="taglist">
		   						   		  <option value="">Select tag</option>	
										  <option value="Mobile health">Mobile health</option>
										  <option value="Research">Research</option>
										  <option value="Trends">Trends</option>
										  <option value="Africa">Africa</option>
										</select>
									</div>
									<div class="columnwidth">
										<input type="text" id="datepicker-from" name="date_range_from" value="" placeholder="MM-DD-YY">
										<div class="select_date_range">
											<a href="javascript:void(0)" id="select_date_range">Select date range</a>
											<input type="text" id="datepicker-to" name="date_range_to" value="" placeholder="MM-DD-YY">
										</div>
										<input type="submit" name="dhi_apply" value="Apply">
									</div>
	   						    </form>
	   						</div><div class="clear"></div>
   					';
   
   $return_string .= '</div>';

   wp_reset_query();
   return $return_string;
}
